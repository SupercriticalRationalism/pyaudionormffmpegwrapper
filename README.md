# pyAudioNormFfmpegWrapper


## Description
A small wrapper for ffmpeg, designed to apply a filter chain to the audio of video or audio files. The aim of this script is to provide easy drag-and-drop audio normalisation and limiting to recorded a/v files used to record streams (vods) where loud, sharp or shrill human screams occur.

The wrapper does the following:
- outputs to the same container as the input, but also prepends the .out extension; e.g. file.mp4 -> file.out.mp4
- copies the video stream verbatim, if it exists;
- applies the filter chain to the audio stream.

The filter chain does the following: 
- Applies a basic de-noise algorithm (afftdn, based on Fast Fourier Transform);
- Resamples the audio up to 4x44.1 kHz (for better audio limiter results);
- Applies dynamic audio normalisation (settings: dynaudnorm=p=0.9:s=5);
- Applies an audio limiter (explained below);
- Resamples the audio down to 44.1 kHz, for expected 'normal' listening.

Limiter settins:
- alimiter=level_in=1:level_out=1:limit=0.1:attack=5:release=50:level=disabled:latency=true
- level_in and level_out maintain the same level;
- limit=0.1: limit the audio loudness to 10% of max output. I found this to slightly lower perceived normal volume, but any higher would not sufficiently bring down startled screams;
- attack=5: 5ms 'response time'; default at the time of writing;
- release=50: 50ms until the limiter 'relaxes'; default at the time of writing;
- level=disabled: disble the auto-volume-leveling simple normaliser; we rely on dynaudnorm for that;
- latency=true: enable the latency-compensation feature of alimiter, so it doesn't introduce additional latency.

## Installation
Ensure ffmpeg is installed. Either add ffmpeg to PATH, or copy this wrapper in the same directory as ffmpeg (or ffmpeg.exe). Ensure python3 is installed.

## Usage
Console: Launch the file in console with one or multiple arguments, each being an a/v file to audio normalise and limit. 
Drag-and-drop: Drag-and-drop multiple such files onto the program.

## License
GPLv3+
