#!/usr/bin/env python3
'''
A wrapper for ffmpeg, to normalise and limit the audio of either pure-audio or audio-video files.
Place in the same directory as ffmpeg, or ensure ffmpeg is in path.
Should work with ffmpeg.exe, too.
'''

import sys
import os
import subprocess

def normalise(pathIn, ffmpeg):
  '''
  Call ffmpeg to process files.
  '''
  pathName, pathExt = os.path.splitext(pathIn)
  pathTmp = pathName + '.tmp' + pathExt
  pathOut = pathName + '.out' + pathExt
  try:
    #Edit audio peak limit here! the limit parameter!
    #Explanation:
    #de-noising,
    #resample audio 4x,
    #dynamically normalise,
    #apply a progressive hard limit (without leveling audio, the dynaudionorm should do that),
    #resample to 44.1 kHz.
    subprocess.run([ffmpeg, '-i', pathIn, '-c:v', 'copy', '-c:a', 'aac', '-filter:a', 'afftdn, aresample=176400, dynaudnorm=p=0.9:s=5, alimiter=level_in=1:level_out=1:limit=0.1:attack=5:release=50:level=disabled:latency=true, aresample=44100', '-y', pathTmp], check = False)
  except:
    return
  os.rename(pathTmp, pathOut)

def findFfmpeg(rootPath):
  '''
  Try to find ffmpeg. If it's not found in local path, attempt PATH, as a fall-back.
  '''
  p = 'ffmpeg'
  if sys.platform.startswith('win'):
    p = p + '.exe'
  localPath = os.path.join(rootPath, p)
  if os.path.isfile(localPath):
    p = localPath
  return p
    
def main(argv):
  '''
  Main wrapper function.
  '''
  rootPath = os.path.dirname(os.path.abspath(argv[0]))
  ffmpeg = findFfmpeg(rootPath)
  for f in argv[1:]:
    try:
      normalise(f, ffmpeg)
    except:
      pass

if __name__ == '__main__':
  main(sys.argv)
